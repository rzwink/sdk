# IO.Swagger.Model.Person
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **Guid?** |  | [optional] 
**FirstName** | **string** |  | 
**LastName** | **string** |  | 
**Employments** | [**List&lt;PersonEmployment&gt;**](PersonEmployment.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

