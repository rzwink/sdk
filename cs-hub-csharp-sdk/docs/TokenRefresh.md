# IO.Swagger.Model.TokenRefresh
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Refresh** | **string** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

