# IO.Swagger.Model.PersonEmployment
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **Guid?** |  | [optional] 
**Employer** | [**Employer**](Employer.md) |  | 
**MonthsOfAvailableData** | **int?** |  | [optional] 
**EstimatedBaseSalary** | **string** |  | 
**PayFrequency** | **string** |  | [optional] 
**Paystatements** | [**List&lt;PersonEmploymentPaystatements&gt;**](PersonEmploymentPaystatements.md) |  | [optional] 
**StartAt** | **DateTime?** |  | [optional] 
**EndAt** | **DateTime?** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

