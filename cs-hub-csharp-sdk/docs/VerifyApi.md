# IO.Swagger.Api.VerifyApi

All URIs are relative to *http://localhost:8000/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**VerifyEmployersCreate**](VerifyApi.md#verifyemployerscreate) | **POST** /verify/employers/ | 
[**VerifyEmployersDelete**](VerifyApi.md#verifyemployersdelete) | **DELETE** /verify/employers/{id}/ | 
[**VerifyEmployersList**](VerifyApi.md#verifyemployerslist) | **GET** /verify/employers/ | 
[**VerifyEmployersPartialUpdate**](VerifyApi.md#verifyemployerspartialupdate) | **PATCH** /verify/employers/{id}/ | 
[**VerifyEmployersRead**](VerifyApi.md#verifyemployersread) | **GET** /verify/employers/{id}/ | 
[**VerifyEmployersUpdate**](VerifyApi.md#verifyemployersupdate) | **PUT** /verify/employers/{id}/ | 
[**VerifyPeopleCreate**](VerifyApi.md#verifypeoplecreate) | **POST** /verify/people/ | 
[**VerifyPeopleDelete**](VerifyApi.md#verifypeopledelete) | **DELETE** /verify/people/{id}/ | 
[**VerifyPeopleList**](VerifyApi.md#verifypeoplelist) | **GET** /verify/people/ | 
[**VerifyPeoplePartialUpdate**](VerifyApi.md#verifypeoplepartialupdate) | **PATCH** /verify/people/{id}/ | 
[**VerifyPeopleRead**](VerifyApi.md#verifypeopleread) | **GET** /verify/people/{id}/ | 
[**VerifyPeopleUpdate**](VerifyApi.md#verifypeopleupdate) | **PUT** /verify/people/{id}/ | 

<a name="verifyemployerscreate"></a>
# **VerifyEmployersCreate**
> Employer VerifyEmployersCreate (Employer body)



API endpoint that allows Persons to be viewed or edited.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class VerifyEmployersCreateExample
    {
        public void main()
        {
            // Configure HTTP basic authorization: Basic
            Configuration.Default.Username = "YOUR_USERNAME";
            Configuration.Default.Password = "YOUR_PASSWORD";

            var apiInstance = new VerifyApi();
            var body = new Employer(); // Employer | 

            try
            {
                Employer result = apiInstance.VerifyEmployersCreate(body);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling VerifyApi.VerifyEmployersCreate: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Employer**](Employer.md)|  | 

### Return type

[**Employer**](Employer.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)
<a name="verifyemployersdelete"></a>
# **VerifyEmployersDelete**
> void VerifyEmployersDelete (Guid? id)



API endpoint that allows Persons to be viewed or edited.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class VerifyEmployersDeleteExample
    {
        public void main()
        {
            // Configure HTTP basic authorization: Basic
            Configuration.Default.Username = "YOUR_USERNAME";
            Configuration.Default.Password = "YOUR_PASSWORD";

            var apiInstance = new VerifyApi();
            var id = new Guid?(); // Guid? | A UUID string identifying this employer.

            try
            {
                apiInstance.VerifyEmployersDelete(id);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling VerifyApi.VerifyEmployersDelete: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**Guid?**](Guid?.md)| A UUID string identifying this employer. | 

### Return type

void (empty response body)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)
<a name="verifyemployerslist"></a>
# **VerifyEmployersList**
> List<Employer> VerifyEmployersList ()



API endpoint that allows Persons to be viewed or edited.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class VerifyEmployersListExample
    {
        public void main()
        {
            // Configure HTTP basic authorization: Basic
            Configuration.Default.Username = "YOUR_USERNAME";
            Configuration.Default.Password = "YOUR_PASSWORD";

            var apiInstance = new VerifyApi();

            try
            {
                List&lt;Employer&gt; result = apiInstance.VerifyEmployersList();
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling VerifyApi.VerifyEmployersList: " + e.Message );
            }
        }
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**List<Employer>**](Employer.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)
<a name="verifyemployerspartialupdate"></a>
# **VerifyEmployersPartialUpdate**
> Employer VerifyEmployersPartialUpdate (Employer body, Guid? id)



API endpoint that allows Persons to be viewed or edited.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class VerifyEmployersPartialUpdateExample
    {
        public void main()
        {
            // Configure HTTP basic authorization: Basic
            Configuration.Default.Username = "YOUR_USERNAME";
            Configuration.Default.Password = "YOUR_PASSWORD";

            var apiInstance = new VerifyApi();
            var body = new Employer(); // Employer | 
            var id = new Guid?(); // Guid? | A UUID string identifying this employer.

            try
            {
                Employer result = apiInstance.VerifyEmployersPartialUpdate(body, id);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling VerifyApi.VerifyEmployersPartialUpdate: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Employer**](Employer.md)|  | 
 **id** | [**Guid?**](Guid?.md)| A UUID string identifying this employer. | 

### Return type

[**Employer**](Employer.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)
<a name="verifyemployersread"></a>
# **VerifyEmployersRead**
> Employer VerifyEmployersRead (Guid? id)



API endpoint that allows Persons to be viewed or edited.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class VerifyEmployersReadExample
    {
        public void main()
        {
            // Configure HTTP basic authorization: Basic
            Configuration.Default.Username = "YOUR_USERNAME";
            Configuration.Default.Password = "YOUR_PASSWORD";

            var apiInstance = new VerifyApi();
            var id = new Guid?(); // Guid? | A UUID string identifying this employer.

            try
            {
                Employer result = apiInstance.VerifyEmployersRead(id);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling VerifyApi.VerifyEmployersRead: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**Guid?**](Guid?.md)| A UUID string identifying this employer. | 

### Return type

[**Employer**](Employer.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)
<a name="verifyemployersupdate"></a>
# **VerifyEmployersUpdate**
> Employer VerifyEmployersUpdate (Employer body, Guid? id)



API endpoint that allows Persons to be viewed or edited.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class VerifyEmployersUpdateExample
    {
        public void main()
        {
            // Configure HTTP basic authorization: Basic
            Configuration.Default.Username = "YOUR_USERNAME";
            Configuration.Default.Password = "YOUR_PASSWORD";

            var apiInstance = new VerifyApi();
            var body = new Employer(); // Employer | 
            var id = new Guid?(); // Guid? | A UUID string identifying this employer.

            try
            {
                Employer result = apiInstance.VerifyEmployersUpdate(body, id);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling VerifyApi.VerifyEmployersUpdate: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Employer**](Employer.md)|  | 
 **id** | [**Guid?**](Guid?.md)| A UUID string identifying this employer. | 

### Return type

[**Employer**](Employer.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)
<a name="verifypeoplecreate"></a>
# **VerifyPeopleCreate**
> Person VerifyPeopleCreate (Person body)



API endpoint that allows Persons to be viewed or edited.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class VerifyPeopleCreateExample
    {
        public void main()
        {
            // Configure HTTP basic authorization: Basic
            Configuration.Default.Username = "YOUR_USERNAME";
            Configuration.Default.Password = "YOUR_PASSWORD";

            var apiInstance = new VerifyApi();
            var body = new Person(); // Person | 

            try
            {
                Person result = apiInstance.VerifyPeopleCreate(body);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling VerifyApi.VerifyPeopleCreate: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Person**](Person.md)|  | 

### Return type

[**Person**](Person.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)
<a name="verifypeopledelete"></a>
# **VerifyPeopleDelete**
> void VerifyPeopleDelete (Guid? id)



API endpoint that allows Persons to be viewed or edited.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class VerifyPeopleDeleteExample
    {
        public void main()
        {
            // Configure HTTP basic authorization: Basic
            Configuration.Default.Username = "YOUR_USERNAME";
            Configuration.Default.Password = "YOUR_PASSWORD";

            var apiInstance = new VerifyApi();
            var id = new Guid?(); // Guid? | A UUID string identifying this person.

            try
            {
                apiInstance.VerifyPeopleDelete(id);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling VerifyApi.VerifyPeopleDelete: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**Guid?**](Guid?.md)| A UUID string identifying this person. | 

### Return type

void (empty response body)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)
<a name="verifypeoplelist"></a>
# **VerifyPeopleList**
> List<Person> VerifyPeopleList ()



API endpoint that allows Persons to be viewed or edited.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class VerifyPeopleListExample
    {
        public void main()
        {
            // Configure HTTP basic authorization: Basic
            Configuration.Default.Username = "YOUR_USERNAME";
            Configuration.Default.Password = "YOUR_PASSWORD";

            var apiInstance = new VerifyApi();

            try
            {
                List&lt;Person&gt; result = apiInstance.VerifyPeopleList();
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling VerifyApi.VerifyPeopleList: " + e.Message );
            }
        }
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**List<Person>**](Person.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)
<a name="verifypeoplepartialupdate"></a>
# **VerifyPeoplePartialUpdate**
> Person VerifyPeoplePartialUpdate (Person body, Guid? id)



API endpoint that allows Persons to be viewed or edited.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class VerifyPeoplePartialUpdateExample
    {
        public void main()
        {
            // Configure HTTP basic authorization: Basic
            Configuration.Default.Username = "YOUR_USERNAME";
            Configuration.Default.Password = "YOUR_PASSWORD";

            var apiInstance = new VerifyApi();
            var body = new Person(); // Person | 
            var id = new Guid?(); // Guid? | A UUID string identifying this person.

            try
            {
                Person result = apiInstance.VerifyPeoplePartialUpdate(body, id);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling VerifyApi.VerifyPeoplePartialUpdate: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Person**](Person.md)|  | 
 **id** | [**Guid?**](Guid?.md)| A UUID string identifying this person. | 

### Return type

[**Person**](Person.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)
<a name="verifypeopleread"></a>
# **VerifyPeopleRead**
> Person VerifyPeopleRead (Guid? id)



API endpoint that allows Persons to be viewed or edited.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class VerifyPeopleReadExample
    {
        public void main()
        {
            // Configure HTTP basic authorization: Basic
            Configuration.Default.Username = "YOUR_USERNAME";
            Configuration.Default.Password = "YOUR_PASSWORD";

            var apiInstance = new VerifyApi();
            var id = new Guid?(); // Guid? | A UUID string identifying this person.

            try
            {
                Person result = apiInstance.VerifyPeopleRead(id);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling VerifyApi.VerifyPeopleRead: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**Guid?**](Guid?.md)| A UUID string identifying this person. | 

### Return type

[**Person**](Person.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)
<a name="verifypeopleupdate"></a>
# **VerifyPeopleUpdate**
> Person VerifyPeopleUpdate (Person body, Guid? id)



API endpoint that allows Persons to be viewed or edited.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class VerifyPeopleUpdateExample
    {
        public void main()
        {
            // Configure HTTP basic authorization: Basic
            Configuration.Default.Username = "YOUR_USERNAME";
            Configuration.Default.Password = "YOUR_PASSWORD";

            var apiInstance = new VerifyApi();
            var body = new Person(); // Person | 
            var id = new Guid?(); // Guid? | A UUID string identifying this person.

            try
            {
                Person result = apiInstance.VerifyPeopleUpdate(body, id);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling VerifyApi.VerifyPeopleUpdate: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Person**](Person.md)|  | 
 **id** | [**Guid?**](Guid?.md)| A UUID string identifying this person. | 

### Return type

[**Person**](Person.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)
