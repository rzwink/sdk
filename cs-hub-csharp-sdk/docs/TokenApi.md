# IO.Swagger.Api.TokenApi

All URIs are relative to *http://localhost:8000/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**TokenCreate**](TokenApi.md#tokencreate) | **POST** /token/ | 
[**TokenRefreshCreate**](TokenApi.md#tokenrefreshcreate) | **POST** /token/refresh/ | 
[**TokenVerifyCreate**](TokenApi.md#tokenverifycreate) | **POST** /token/verify/ | 

<a name="tokencreate"></a>
# **TokenCreate**
> TokenObtainPair TokenCreate (TokenObtainPair body)



Takes a set of user credentials and returns an access and refresh JSON web token pair to prove the authentication of those credentials.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class TokenCreateExample
    {
        public void main()
        {
            // Configure HTTP basic authorization: Basic
            Configuration.Default.Username = "YOUR_USERNAME";
            Configuration.Default.Password = "YOUR_PASSWORD";

            var apiInstance = new TokenApi();
            var body = new TokenObtainPair(); // TokenObtainPair | 

            try
            {
                TokenObtainPair result = apiInstance.TokenCreate(body);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling TokenApi.TokenCreate: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**TokenObtainPair**](TokenObtainPair.md)|  | 

### Return type

[**TokenObtainPair**](TokenObtainPair.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)
<a name="tokenrefreshcreate"></a>
# **TokenRefreshCreate**
> TokenRefresh TokenRefreshCreate (TokenRefresh body)



Takes a refresh type JSON web token and returns an access type JSON web token if the refresh token is valid.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class TokenRefreshCreateExample
    {
        public void main()
        {
            // Configure HTTP basic authorization: Basic
            Configuration.Default.Username = "YOUR_USERNAME";
            Configuration.Default.Password = "YOUR_PASSWORD";

            var apiInstance = new TokenApi();
            var body = new TokenRefresh(); // TokenRefresh | 

            try
            {
                TokenRefresh result = apiInstance.TokenRefreshCreate(body);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling TokenApi.TokenRefreshCreate: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**TokenRefresh**](TokenRefresh.md)|  | 

### Return type

[**TokenRefresh**](TokenRefresh.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)
<a name="tokenverifycreate"></a>
# **TokenVerifyCreate**
> TokenVerify TokenVerifyCreate (TokenVerify body)



Takes a token and indicates if it is valid.  This view provides no information about a token's fitness for a particular use.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class TokenVerifyCreateExample
    {
        public void main()
        {
            // Configure HTTP basic authorization: Basic
            Configuration.Default.Username = "YOUR_USERNAME";
            Configuration.Default.Password = "YOUR_PASSWORD";

            var apiInstance = new TokenApi();
            var body = new TokenVerify(); // TokenVerify | 

            try
            {
                TokenVerify result = apiInstance.TokenVerifyCreate(body);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling TokenApi.TokenVerifyCreate: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**TokenVerify**](TokenVerify.md)|  | 

### Return type

[**TokenVerify**](TokenVerify.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)
