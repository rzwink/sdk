# IO.Swagger.Model.PersonEmploymentPaystatements
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **Guid?** |  | [optional] 
**PayDate** | **DateTime?** |  | 
**PayPeriodStart** | **DateTime?** |  | [optional] 
**PayPeriodEnd** | **DateTime?** |  | [optional] 
**NetPayAmountCurrency** | **string** |  | [optional] 
**NetPayAmount** | **string** |  | [optional] 
**GrossPayAmountCurrency** | **string** |  | [optional] 
**GrossPayAmount** | **string** |  | [optional] 
**GrossPayYtdAmountCurrency** | **string** |  | [optional] 
**GrossPayYtdAmount** | **string** |  | [optional] 
**TotalHours** | **int?** |  | 
**CreatedAt** | **DateTime?** |  | [optional] 
**UpdatedAt** | **DateTime?** |  | [optional] 
**Employment** | **Guid?** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

