//
// TokenAPI.swift
//
// Generated by swagger-codegen
// https://github.com/swagger-api/swagger-codegen
//

import Foundation
import Alamofire


open class TokenAPI {
    /**

     - parameter body: (body)  
     - parameter completion: completion handler to receive the data and the error objects
     */
    open class func tokenCreate(body: TokenObtainPair, completion: @escaping ((_ data: TokenObtainPair?,_ error: Error?) -> Void)) {
        tokenCreateWithRequestBuilder(body: body).execute { (response, error) -> Void in
            completion(response?.body, error)
        }
    }


    /**
     - POST /token/

     - BASIC:
       - type: http
       - name: Basic
     - examples: [{contentType=application/json, example={
  "password" : "password",
  "username" : "username"
}}]
     - parameter body: (body)  

     - returns: RequestBuilder<TokenObtainPair> 
     */
    open class func tokenCreateWithRequestBuilder(body: TokenObtainPair) -> RequestBuilder<TokenObtainPair> {
        let path = "/token/"
        let URLString = SwaggerClientAPI.basePath + path
        let parameters = JSONEncodingHelper.encodingParameters(forEncodableObject: body)

        let url = URLComponents(string: URLString)

        let requestBuilder: RequestBuilder<TokenObtainPair>.Type = SwaggerClientAPI.requestBuilderFactory.getBuilder()

        return requestBuilder.init(method: "POST", URLString: (url?.string ?? URLString), parameters: parameters, isBody: true)
    }

    /**

     - parameter body: (body)  
     - parameter completion: completion handler to receive the data and the error objects
     */
    open class func tokenRefreshCreate(body: TokenRefresh, completion: @escaping ((_ data: TokenRefresh?,_ error: Error?) -> Void)) {
        tokenRefreshCreateWithRequestBuilder(body: body).execute { (response, error) -> Void in
            completion(response?.body, error)
        }
    }


    /**
     - POST /token/refresh/

     - BASIC:
       - type: http
       - name: Basic
     - examples: [{contentType=application/json, example={
  "refresh" : "refresh"
}}]
     - parameter body: (body)  

     - returns: RequestBuilder<TokenRefresh> 
     */
    open class func tokenRefreshCreateWithRequestBuilder(body: TokenRefresh) -> RequestBuilder<TokenRefresh> {
        let path = "/token/refresh/"
        let URLString = SwaggerClientAPI.basePath + path
        let parameters = JSONEncodingHelper.encodingParameters(forEncodableObject: body)

        let url = URLComponents(string: URLString)

        let requestBuilder: RequestBuilder<TokenRefresh>.Type = SwaggerClientAPI.requestBuilderFactory.getBuilder()

        return requestBuilder.init(method: "POST", URLString: (url?.string ?? URLString), parameters: parameters, isBody: true)
    }

    /**

     - parameter body: (body)  
     - parameter completion: completion handler to receive the data and the error objects
     */
    open class func tokenVerifyCreate(body: TokenVerify, completion: @escaping ((_ data: TokenVerify?,_ error: Error?) -> Void)) {
        tokenVerifyCreateWithRequestBuilder(body: body).execute { (response, error) -> Void in
            completion(response?.body, error)
        }
    }


    /**
     - POST /token/verify/

     - BASIC:
       - type: http
       - name: Basic
     - examples: [{contentType=application/json, example={
  "token" : "token"
}}]
     - parameter body: (body)  

     - returns: RequestBuilder<TokenVerify> 
     */
    open class func tokenVerifyCreateWithRequestBuilder(body: TokenVerify) -> RequestBuilder<TokenVerify> {
        let path = "/token/verify/"
        let URLString = SwaggerClientAPI.basePath + path
        let parameters = JSONEncodingHelper.encodingParameters(forEncodableObject: body)

        let url = URLComponents(string: URLString)

        let requestBuilder: RequestBuilder<TokenVerify>.Type = SwaggerClientAPI.requestBuilderFactory.getBuilder()

        return requestBuilder.init(method: "POST", URLString: (url?.string ?? URLString), parameters: parameters, isBody: true)
    }

}
