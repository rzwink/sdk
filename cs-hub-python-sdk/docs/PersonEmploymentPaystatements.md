# PersonEmploymentPaystatements

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** |  | [optional] 
**pay_date** | **date** |  | 
**pay_period_start** | **date** |  | [optional] 
**pay_period_end** | **date** |  | [optional] 
**net_pay_amount_currency** | **str** |  | [optional] 
**net_pay_amount** | **str** |  | [optional] 
**gross_pay_amount_currency** | **str** |  | [optional] 
**gross_pay_amount** | **str** |  | [optional] 
**gross_pay_ytd_amount_currency** | **str** |  | [optional] 
**gross_pay_ytd_amount** | **str** |  | [optional] 
**total_hours** | **int** |  | 
**created_at** | **datetime** |  | [optional] 
**updated_at** | **datetime** |  | [optional] 
**employment** | **str** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

