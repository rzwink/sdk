# swagger_client.VerifyApi

All URIs are relative to *http://localhost:8000/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**verify_employers_create**](VerifyApi.md#verify_employers_create) | **POST** /verify/employers/ | 
[**verify_employers_delete**](VerifyApi.md#verify_employers_delete) | **DELETE** /verify/employers/{id}/ | 
[**verify_employers_list**](VerifyApi.md#verify_employers_list) | **GET** /verify/employers/ | 
[**verify_employers_partial_update**](VerifyApi.md#verify_employers_partial_update) | **PATCH** /verify/employers/{id}/ | 
[**verify_employers_read**](VerifyApi.md#verify_employers_read) | **GET** /verify/employers/{id}/ | 
[**verify_employers_update**](VerifyApi.md#verify_employers_update) | **PUT** /verify/employers/{id}/ | 
[**verify_people_create**](VerifyApi.md#verify_people_create) | **POST** /verify/people/ | 
[**verify_people_delete**](VerifyApi.md#verify_people_delete) | **DELETE** /verify/people/{id}/ | 
[**verify_people_list**](VerifyApi.md#verify_people_list) | **GET** /verify/people/ | 
[**verify_people_partial_update**](VerifyApi.md#verify_people_partial_update) | **PATCH** /verify/people/{id}/ | 
[**verify_people_read**](VerifyApi.md#verify_people_read) | **GET** /verify/people/{id}/ | 
[**verify_people_update**](VerifyApi.md#verify_people_update) | **PUT** /verify/people/{id}/ | 

# **verify_employers_create**
> Employer verify_employers_create(body)



API endpoint that allows Persons to be viewed or edited.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint
# Configure HTTP basic authorization: Basic
configuration = swagger_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = swagger_client.VerifyApi(swagger_client.ApiClient(configuration))
body = swagger_client.Employer() # Employer | 

try:
    api_response = api_instance.verify_employers_create(body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling VerifyApi->verify_employers_create: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Employer**](Employer.md)|  | 

### Return type

[**Employer**](Employer.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **verify_employers_delete**
> verify_employers_delete(id)



API endpoint that allows Persons to be viewed or edited.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint
# Configure HTTP basic authorization: Basic
configuration = swagger_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = swagger_client.VerifyApi(swagger_client.ApiClient(configuration))
id = '38400000-8cf0-11bd-b23e-10b96e4ef00d' # str | A UUID string identifying this employer.

try:
    api_instance.verify_employers_delete(id)
except ApiException as e:
    print("Exception when calling VerifyApi->verify_employers_delete: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**str**](.md)| A UUID string identifying this employer. | 

### Return type

void (empty response body)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **verify_employers_list**
> list[Employer] verify_employers_list()



API endpoint that allows Persons to be viewed or edited.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint
# Configure HTTP basic authorization: Basic
configuration = swagger_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = swagger_client.VerifyApi(swagger_client.ApiClient(configuration))

try:
    api_response = api_instance.verify_employers_list()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling VerifyApi->verify_employers_list: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**list[Employer]**](Employer.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **verify_employers_partial_update**
> Employer verify_employers_partial_update(body, id)



API endpoint that allows Persons to be viewed or edited.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint
# Configure HTTP basic authorization: Basic
configuration = swagger_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = swagger_client.VerifyApi(swagger_client.ApiClient(configuration))
body = swagger_client.Employer() # Employer | 
id = '38400000-8cf0-11bd-b23e-10b96e4ef00d' # str | A UUID string identifying this employer.

try:
    api_response = api_instance.verify_employers_partial_update(body, id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling VerifyApi->verify_employers_partial_update: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Employer**](Employer.md)|  | 
 **id** | [**str**](.md)| A UUID string identifying this employer. | 

### Return type

[**Employer**](Employer.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **verify_employers_read**
> Employer verify_employers_read(id)



API endpoint that allows Persons to be viewed or edited.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint
# Configure HTTP basic authorization: Basic
configuration = swagger_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = swagger_client.VerifyApi(swagger_client.ApiClient(configuration))
id = '38400000-8cf0-11bd-b23e-10b96e4ef00d' # str | A UUID string identifying this employer.

try:
    api_response = api_instance.verify_employers_read(id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling VerifyApi->verify_employers_read: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**str**](.md)| A UUID string identifying this employer. | 

### Return type

[**Employer**](Employer.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **verify_employers_update**
> Employer verify_employers_update(body, id)



API endpoint that allows Persons to be viewed or edited.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint
# Configure HTTP basic authorization: Basic
configuration = swagger_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = swagger_client.VerifyApi(swagger_client.ApiClient(configuration))
body = swagger_client.Employer() # Employer | 
id = '38400000-8cf0-11bd-b23e-10b96e4ef00d' # str | A UUID string identifying this employer.

try:
    api_response = api_instance.verify_employers_update(body, id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling VerifyApi->verify_employers_update: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Employer**](Employer.md)|  | 
 **id** | [**str**](.md)| A UUID string identifying this employer. | 

### Return type

[**Employer**](Employer.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **verify_people_create**
> Person verify_people_create(body)



API endpoint that allows Persons to be viewed or edited.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint
# Configure HTTP basic authorization: Basic
configuration = swagger_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = swagger_client.VerifyApi(swagger_client.ApiClient(configuration))
body = swagger_client.Person() # Person | 

try:
    api_response = api_instance.verify_people_create(body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling VerifyApi->verify_people_create: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Person**](Person.md)|  | 

### Return type

[**Person**](Person.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **verify_people_delete**
> verify_people_delete(id)



API endpoint that allows Persons to be viewed or edited.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint
# Configure HTTP basic authorization: Basic
configuration = swagger_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = swagger_client.VerifyApi(swagger_client.ApiClient(configuration))
id = '38400000-8cf0-11bd-b23e-10b96e4ef00d' # str | A UUID string identifying this person.

try:
    api_instance.verify_people_delete(id)
except ApiException as e:
    print("Exception when calling VerifyApi->verify_people_delete: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**str**](.md)| A UUID string identifying this person. | 

### Return type

void (empty response body)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **verify_people_list**
> list[Person] verify_people_list()



API endpoint that allows Persons to be viewed or edited.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint
# Configure HTTP basic authorization: Basic
configuration = swagger_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = swagger_client.VerifyApi(swagger_client.ApiClient(configuration))

try:
    api_response = api_instance.verify_people_list()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling VerifyApi->verify_people_list: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**list[Person]**](Person.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **verify_people_partial_update**
> Person verify_people_partial_update(body, id)



API endpoint that allows Persons to be viewed or edited.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint
# Configure HTTP basic authorization: Basic
configuration = swagger_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = swagger_client.VerifyApi(swagger_client.ApiClient(configuration))
body = swagger_client.Person() # Person | 
id = '38400000-8cf0-11bd-b23e-10b96e4ef00d' # str | A UUID string identifying this person.

try:
    api_response = api_instance.verify_people_partial_update(body, id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling VerifyApi->verify_people_partial_update: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Person**](Person.md)|  | 
 **id** | [**str**](.md)| A UUID string identifying this person. | 

### Return type

[**Person**](Person.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **verify_people_read**
> Person verify_people_read(id)



API endpoint that allows Persons to be viewed or edited.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint
# Configure HTTP basic authorization: Basic
configuration = swagger_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = swagger_client.VerifyApi(swagger_client.ApiClient(configuration))
id = '38400000-8cf0-11bd-b23e-10b96e4ef00d' # str | A UUID string identifying this person.

try:
    api_response = api_instance.verify_people_read(id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling VerifyApi->verify_people_read: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**str**](.md)| A UUID string identifying this person. | 

### Return type

[**Person**](Person.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **verify_people_update**
> Person verify_people_update(body, id)



API endpoint that allows Persons to be viewed or edited.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint
# Configure HTTP basic authorization: Basic
configuration = swagger_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = swagger_client.VerifyApi(swagger_client.ApiClient(configuration))
body = swagger_client.Person() # Person | 
id = '38400000-8cf0-11bd-b23e-10b96e4ef00d' # str | A UUID string identifying this person.

try:
    api_response = api_instance.verify_people_update(body, id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling VerifyApi->verify_people_update: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Person**](Person.md)|  | 
 **id** | [**str**](.md)| A UUID string identifying this person. | 

### Return type

[**Person**](Person.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

