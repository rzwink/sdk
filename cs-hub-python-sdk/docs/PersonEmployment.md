# PersonEmployment

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** |  | [optional] 
**employer** | [**Employer**](Employer.md) |  | 
**months_of_available_data** | **int** |  | [optional] 
**estimated_base_salary** | **str** |  | 
**pay_frequency** | **str** |  | [optional] 
**paystatements** | [**list[PersonEmploymentPaystatements]**](PersonEmploymentPaystatements.md) |  | [optional] 
**start_at** | **date** |  | [optional] 
**end_at** | **date** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

