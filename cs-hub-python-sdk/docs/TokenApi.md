# swagger_client.TokenApi

All URIs are relative to *http://localhost:8000/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**token_create**](TokenApi.md#token_create) | **POST** /token/ | 
[**token_refresh_create**](TokenApi.md#token_refresh_create) | **POST** /token/refresh/ | 
[**token_verify_create**](TokenApi.md#token_verify_create) | **POST** /token/verify/ | 

# **token_create**
> TokenObtainPair token_create(body)



Takes a set of user credentials and returns an access and refresh JSON web token pair to prove the authentication of those credentials.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint
# Configure HTTP basic authorization: Basic
configuration = swagger_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = swagger_client.TokenApi(swagger_client.ApiClient(configuration))
body = swagger_client.TokenObtainPair() # TokenObtainPair | 

try:
    api_response = api_instance.token_create(body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling TokenApi->token_create: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**TokenObtainPair**](TokenObtainPair.md)|  | 

### Return type

[**TokenObtainPair**](TokenObtainPair.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **token_refresh_create**
> TokenRefresh token_refresh_create(body)



Takes a refresh type JSON web token and returns an access type JSON web token if the refresh token is valid.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint
# Configure HTTP basic authorization: Basic
configuration = swagger_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = swagger_client.TokenApi(swagger_client.ApiClient(configuration))
body = swagger_client.TokenRefresh() # TokenRefresh | 

try:
    api_response = api_instance.token_refresh_create(body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling TokenApi->token_refresh_create: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**TokenRefresh**](TokenRefresh.md)|  | 

### Return type

[**TokenRefresh**](TokenRefresh.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **token_verify_create**
> TokenVerify token_verify_create(body)



Takes a token and indicates if it is valid.  This view provides no information about a token's fitness for a particular use.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint
# Configure HTTP basic authorization: Basic
configuration = swagger_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = swagger_client.TokenApi(swagger_client.ApiClient(configuration))
body = swagger_client.TokenVerify() # TokenVerify | 

try:
    api_response = api_instance.token_verify_create(body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling TokenApi->token_verify_create: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**TokenVerify**](TokenVerify.md)|  | 

### Return type

[**TokenVerify**](TokenVerify.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

