# coding: utf-8

"""
    ClickSWITCH Hub API

    ClickSWITCH Hub provides functionality for account switching, account closure, employment and income verification.  # noqa: E501

    OpenAPI spec version: v1
    Contact: support@clickswitch.com
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

from __future__ import absolute_import

import unittest

import swagger_client
from swagger_client.models.employer import Employer  # noqa: E501
from swagger_client.rest import ApiException


class TestEmployer(unittest.TestCase):
    """Employer unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testEmployer(self):
        """Test Employer"""
        # FIXME: construct object with mandatory attributes with example values
        # model = swagger_client.models.employer.Employer()  # noqa: E501
        pass


if __name__ == '__main__':
    unittest.main()
