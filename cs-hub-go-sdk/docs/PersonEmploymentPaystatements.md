# PersonEmploymentPaystatements

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** |  | [optional] [default to null]
**PayDate** | **string** |  | [default to null]
**PayPeriodStart** | **string** |  | [optional] [default to null]
**PayPeriodEnd** | **string** |  | [optional] [default to null]
**NetPayAmountCurrency** | **string** |  | [optional] [default to null]
**NetPayAmount** | **string** |  | [optional] [default to null]
**GrossPayAmountCurrency** | **string** |  | [optional] [default to null]
**GrossPayAmount** | **string** |  | [optional] [default to null]
**GrossPayYtdAmountCurrency** | **string** |  | [optional] [default to null]
**GrossPayYtdAmount** | **string** |  | [optional] [default to null]
**TotalHours** | **int32** |  | [default to null]
**CreatedAt** | [**time.Time**](time.Time.md) |  | [optional] [default to null]
**UpdatedAt** | [**time.Time**](time.Time.md) |  | [optional] [default to null]
**Employment** | **string** |  | [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

