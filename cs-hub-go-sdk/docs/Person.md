# Person

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** |  | [optional] [default to null]
**FirstName** | **string** |  | [default to null]
**LastName** | **string** |  | [default to null]
**Employments** | [**[]PersonEmployment**](PersonEmployment.md) |  | [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

