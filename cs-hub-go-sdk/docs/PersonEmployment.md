# PersonEmployment

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** |  | [optional] [default to null]
**Employer** | [***Employer**](Employer.md) |  | [default to null]
**MonthsOfAvailableData** | **int32** |  | [optional] [default to null]
**EstimatedBaseSalary** | **string** |  | [default to null]
**PayFrequency** | **string** |  | [optional] [default to null]
**Paystatements** | [**[]PersonEmploymentPaystatements**](PersonEmployment_paystatements.md) |  | [optional] [default to null]
**StartAt** | **string** |  | [optional] [default to null]
**EndAt** | **string** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

