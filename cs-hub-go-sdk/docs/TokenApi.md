# {{classname}}

All URIs are relative to *http://localhost:8000/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**TokenCreate**](TokenApi.md#TokenCreate) | **Post** /token/ | 
[**TokenRefreshCreate**](TokenApi.md#TokenRefreshCreate) | **Post** /token/refresh/ | 
[**TokenVerifyCreate**](TokenApi.md#TokenVerifyCreate) | **Post** /token/verify/ | 

# **TokenCreate**
> TokenObtainPair TokenCreate(ctx, body)


Takes a set of user credentials and returns an access and refresh JSON web token pair to prove the authentication of those credentials.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **body** | [**TokenObtainPair**](TokenObtainPair.md)|  | 

### Return type

[**TokenObtainPair**](TokenObtainPair.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **TokenRefreshCreate**
> TokenRefresh TokenRefreshCreate(ctx, body)


Takes a refresh type JSON web token and returns an access type JSON web token if the refresh token is valid.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **body** | [**TokenRefresh**](TokenRefresh.md)|  | 

### Return type

[**TokenRefresh**](TokenRefresh.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **TokenVerifyCreate**
> TokenVerify TokenVerifyCreate(ctx, body)


Takes a token and indicates if it is valid.  This view provides no information about a token's fitness for a particular use.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **body** | [**TokenVerify**](TokenVerify.md)|  | 

### Return type

[**TokenVerify**](TokenVerify.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

