# {{classname}}

All URIs are relative to *http://localhost:8000/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**VerifyEmployersCreate**](VerifyApi.md#VerifyEmployersCreate) | **Post** /verify/employers/ | 
[**VerifyEmployersDelete**](VerifyApi.md#VerifyEmployersDelete) | **Delete** /verify/employers/{id}/ | 
[**VerifyEmployersList**](VerifyApi.md#VerifyEmployersList) | **Get** /verify/employers/ | 
[**VerifyEmployersPartialUpdate**](VerifyApi.md#VerifyEmployersPartialUpdate) | **Patch** /verify/employers/{id}/ | 
[**VerifyEmployersRead**](VerifyApi.md#VerifyEmployersRead) | **Get** /verify/employers/{id}/ | 
[**VerifyEmployersUpdate**](VerifyApi.md#VerifyEmployersUpdate) | **Put** /verify/employers/{id}/ | 
[**VerifyPeopleCreate**](VerifyApi.md#VerifyPeopleCreate) | **Post** /verify/people/ | 
[**VerifyPeopleDelete**](VerifyApi.md#VerifyPeopleDelete) | **Delete** /verify/people/{id}/ | 
[**VerifyPeopleList**](VerifyApi.md#VerifyPeopleList) | **Get** /verify/people/ | 
[**VerifyPeoplePartialUpdate**](VerifyApi.md#VerifyPeoplePartialUpdate) | **Patch** /verify/people/{id}/ | 
[**VerifyPeopleRead**](VerifyApi.md#VerifyPeopleRead) | **Get** /verify/people/{id}/ | 
[**VerifyPeopleUpdate**](VerifyApi.md#VerifyPeopleUpdate) | **Put** /verify/people/{id}/ | 

# **VerifyEmployersCreate**
> Employer VerifyEmployersCreate(ctx, body)


API endpoint that allows Persons to be viewed or edited.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **body** | [**Employer**](Employer.md)|  | 

### Return type

[**Employer**](Employer.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **VerifyEmployersDelete**
> VerifyEmployersDelete(ctx, id)


API endpoint that allows Persons to be viewed or edited.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **id** | [**string**](.md)| A UUID string identifying this employer. | 

### Return type

 (empty response body)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **VerifyEmployersList**
> []Employer VerifyEmployersList(ctx, )


API endpoint that allows Persons to be viewed or edited.

### Required Parameters
This endpoint does not need any parameter.

### Return type

[**[]Employer**](Employer.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **VerifyEmployersPartialUpdate**
> Employer VerifyEmployersPartialUpdate(ctx, body, id)


API endpoint that allows Persons to be viewed or edited.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **body** | [**Employer**](Employer.md)|  | 
  **id** | [**string**](.md)| A UUID string identifying this employer. | 

### Return type

[**Employer**](Employer.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **VerifyEmployersRead**
> Employer VerifyEmployersRead(ctx, id)


API endpoint that allows Persons to be viewed or edited.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **id** | [**string**](.md)| A UUID string identifying this employer. | 

### Return type

[**Employer**](Employer.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **VerifyEmployersUpdate**
> Employer VerifyEmployersUpdate(ctx, body, id)


API endpoint that allows Persons to be viewed or edited.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **body** | [**Employer**](Employer.md)|  | 
  **id** | [**string**](.md)| A UUID string identifying this employer. | 

### Return type

[**Employer**](Employer.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **VerifyPeopleCreate**
> Person VerifyPeopleCreate(ctx, body)


API endpoint that allows Persons to be viewed or edited.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **body** | [**Person**](Person.md)|  | 

### Return type

[**Person**](Person.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **VerifyPeopleDelete**
> VerifyPeopleDelete(ctx, id)


API endpoint that allows Persons to be viewed or edited.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **id** | [**string**](.md)| A UUID string identifying this person. | 

### Return type

 (empty response body)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **VerifyPeopleList**
> []Person VerifyPeopleList(ctx, )


API endpoint that allows Persons to be viewed or edited.

### Required Parameters
This endpoint does not need any parameter.

### Return type

[**[]Person**](Person.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **VerifyPeoplePartialUpdate**
> Person VerifyPeoplePartialUpdate(ctx, body, id)


API endpoint that allows Persons to be viewed or edited.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **body** | [**Person**](Person.md)|  | 
  **id** | [**string**](.md)| A UUID string identifying this person. | 

### Return type

[**Person**](Person.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **VerifyPeopleRead**
> Person VerifyPeopleRead(ctx, id)


API endpoint that allows Persons to be viewed or edited.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **id** | [**string**](.md)| A UUID string identifying this person. | 

### Return type

[**Person**](Person.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **VerifyPeopleUpdate**
> Person VerifyPeopleUpdate(ctx, body, id)


API endpoint that allows Persons to be viewed or edited.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **body** | [**Person**](Person.md)|  | 
  **id** | [**string**](.md)| A UUID string identifying this person. | 

### Return type

[**Person**](Person.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

