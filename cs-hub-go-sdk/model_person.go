/*
 * ClickSWITCH Hub API
 *
 * ClickSWITCH Hub provides functionality for account switching, account closure, employment and income verification.
 *
 * API version: v1
 * Contact: support@clickswitch.com
 * Generated by: Swagger Codegen (https://github.com/swagger-api/swagger-codegen.git)
 */
package swagger

type Person struct {
	Id string `json:"id,omitempty"`
	FirstName string `json:"first_name"`
	LastName string `json:"last_name"`
	Employments []PersonEmployment `json:"employments"`
}
