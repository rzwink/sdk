/*
 * ClickSWITCH Hub API
 * ClickSWITCH Hub provides functionality for account switching, account closure, employment and income verification.
 *
 * OpenAPI spec version: v1
 * Contact: support@clickswitch.com
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */

package io.swagger.client.auth;

public enum OAuthFlow {
    accessCode, implicit, password, application
}
