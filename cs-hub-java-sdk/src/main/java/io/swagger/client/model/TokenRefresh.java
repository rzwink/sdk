/*
 * ClickSWITCH Hub API
 * ClickSWITCH Hub provides functionality for account switching, account closure, employment and income verification.
 *
 * OpenAPI spec version: v1
 * Contact: support@clickswitch.com
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */

package io.swagger.client.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.v3.oas.annotations.media.Schema;
import java.io.IOException;
/**
 * TokenRefresh
 */

@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.JavaClientCodegen", date = "2020-08-25T07:46:58.399-05:00[America/Chicago]")
public class TokenRefresh {
  @SerializedName("refresh")
  private String refresh = null;

  public TokenRefresh refresh(String refresh) {
    this.refresh = refresh;
    return this;
  }

   /**
   * Get refresh
   * @return refresh
  **/
  @Schema(required = true, description = "")
  public String getRefresh() {
    return refresh;
  }

  public void setRefresh(String refresh) {
    this.refresh = refresh;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    TokenRefresh tokenRefresh = (TokenRefresh) o;
    return Objects.equals(this.refresh, tokenRefresh.refresh);
  }

  @Override
  public int hashCode() {
    return Objects.hash(refresh);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class TokenRefresh {\n");
    
    sb.append("    refresh: ").append(toIndentedString(refresh)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}
