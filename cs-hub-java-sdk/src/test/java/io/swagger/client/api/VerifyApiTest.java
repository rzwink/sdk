/*
 * ClickSWITCH Hub API
 * ClickSWITCH Hub provides functionality for account switching, account closure, employment and income verification.
 *
 * OpenAPI spec version: v1
 * Contact: support@clickswitch.com
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */

package io.swagger.client.api;

import io.swagger.client.ApiException;
import io.swagger.client.model.Employer;
import io.swagger.client.model.Person;
import java.util.UUID;
import org.junit.Test;
import org.junit.Ignore;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * API tests for VerifyApi
 */
@Ignore
public class VerifyApiTest {

    private final VerifyApi api = new VerifyApi();

    /**
     * 
     *
     * API endpoint that allows Persons to be viewed or edited.
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void verifyEmployersCreateTest() throws ApiException {
        Employer body = null;
        Employer response = api.verifyEmployersCreate(body);

        // TODO: test validations
    }
    /**
     * 
     *
     * API endpoint that allows Persons to be viewed or edited.
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void verifyEmployersDeleteTest() throws ApiException {
        UUID id = null;
        api.verifyEmployersDelete(id);

        // TODO: test validations
    }
    /**
     * 
     *
     * API endpoint that allows Persons to be viewed or edited.
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void verifyEmployersListTest() throws ApiException {
        List<Employer> response = api.verifyEmployersList();

        // TODO: test validations
    }
    /**
     * 
     *
     * API endpoint that allows Persons to be viewed or edited.
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void verifyEmployersPartialUpdateTest() throws ApiException {
        Employer body = null;
        UUID id = null;
        Employer response = api.verifyEmployersPartialUpdate(body, id);

        // TODO: test validations
    }
    /**
     * 
     *
     * API endpoint that allows Persons to be viewed or edited.
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void verifyEmployersReadTest() throws ApiException {
        UUID id = null;
        Employer response = api.verifyEmployersRead(id);

        // TODO: test validations
    }
    /**
     * 
     *
     * API endpoint that allows Persons to be viewed or edited.
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void verifyEmployersUpdateTest() throws ApiException {
        Employer body = null;
        UUID id = null;
        Employer response = api.verifyEmployersUpdate(body, id);

        // TODO: test validations
    }
    /**
     * 
     *
     * API endpoint that allows Persons to be viewed or edited.
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void verifyPeopleCreateTest() throws ApiException {
        Person body = null;
        Person response = api.verifyPeopleCreate(body);

        // TODO: test validations
    }
    /**
     * 
     *
     * API endpoint that allows Persons to be viewed or edited.
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void verifyPeopleDeleteTest() throws ApiException {
        UUID id = null;
        api.verifyPeopleDelete(id);

        // TODO: test validations
    }
    /**
     * 
     *
     * API endpoint that allows Persons to be viewed or edited.
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void verifyPeopleListTest() throws ApiException {
        List<Person> response = api.verifyPeopleList();

        // TODO: test validations
    }
    /**
     * 
     *
     * API endpoint that allows Persons to be viewed or edited.
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void verifyPeoplePartialUpdateTest() throws ApiException {
        Person body = null;
        UUID id = null;
        Person response = api.verifyPeoplePartialUpdate(body, id);

        // TODO: test validations
    }
    /**
     * 
     *
     * API endpoint that allows Persons to be viewed or edited.
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void verifyPeopleReadTest() throws ApiException {
        UUID id = null;
        Person response = api.verifyPeopleRead(id);

        // TODO: test validations
    }
    /**
     * 
     *
     * API endpoint that allows Persons to be viewed or edited.
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void verifyPeopleUpdateTest() throws ApiException {
        Person body = null;
        UUID id = null;
        Person response = api.verifyPeopleUpdate(body, id);

        // TODO: test validations
    }
}
