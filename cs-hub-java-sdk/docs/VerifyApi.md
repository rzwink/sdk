# VerifyApi

All URIs are relative to *http://localhost:8000/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**verifyEmployersCreate**](VerifyApi.md#verifyEmployersCreate) | **POST** /verify/employers/ | 
[**verifyEmployersDelete**](VerifyApi.md#verifyEmployersDelete) | **DELETE** /verify/employers/{id}/ | 
[**verifyEmployersList**](VerifyApi.md#verifyEmployersList) | **GET** /verify/employers/ | 
[**verifyEmployersPartialUpdate**](VerifyApi.md#verifyEmployersPartialUpdate) | **PATCH** /verify/employers/{id}/ | 
[**verifyEmployersRead**](VerifyApi.md#verifyEmployersRead) | **GET** /verify/employers/{id}/ | 
[**verifyEmployersUpdate**](VerifyApi.md#verifyEmployersUpdate) | **PUT** /verify/employers/{id}/ | 
[**verifyPeopleCreate**](VerifyApi.md#verifyPeopleCreate) | **POST** /verify/people/ | 
[**verifyPeopleDelete**](VerifyApi.md#verifyPeopleDelete) | **DELETE** /verify/people/{id}/ | 
[**verifyPeopleList**](VerifyApi.md#verifyPeopleList) | **GET** /verify/people/ | 
[**verifyPeoplePartialUpdate**](VerifyApi.md#verifyPeoplePartialUpdate) | **PATCH** /verify/people/{id}/ | 
[**verifyPeopleRead**](VerifyApi.md#verifyPeopleRead) | **GET** /verify/people/{id}/ | 
[**verifyPeopleUpdate**](VerifyApi.md#verifyPeopleUpdate) | **PUT** /verify/people/{id}/ | 

<a name="verifyEmployersCreate"></a>
# **verifyEmployersCreate**
> Employer verifyEmployersCreate(body)



API endpoint that allows Persons to be viewed or edited.

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.VerifyApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();
// Configure HTTP basic authorization: Basic
HttpBasicAuth Basic = (HttpBasicAuth) defaultClient.getAuthentication("Basic");
Basic.setUsername("YOUR USERNAME");
Basic.setPassword("YOUR PASSWORD");

VerifyApi apiInstance = new VerifyApi();
Employer body = new Employer(); // Employer | 
try {
    Employer result = apiInstance.verifyEmployersCreate(body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling VerifyApi#verifyEmployersCreate");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Employer**](Employer.md)|  |

### Return type

[**Employer**](Employer.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="verifyEmployersDelete"></a>
# **verifyEmployersDelete**
> verifyEmployersDelete(id)



API endpoint that allows Persons to be viewed or edited.

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.VerifyApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();
// Configure HTTP basic authorization: Basic
HttpBasicAuth Basic = (HttpBasicAuth) defaultClient.getAuthentication("Basic");
Basic.setUsername("YOUR USERNAME");
Basic.setPassword("YOUR PASSWORD");

VerifyApi apiInstance = new VerifyApi();
UUID id = new UUID(); // UUID | A UUID string identifying this employer.
try {
    apiInstance.verifyEmployersDelete(id);
} catch (ApiException e) {
    System.err.println("Exception when calling VerifyApi#verifyEmployersDelete");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**UUID**](.md)| A UUID string identifying this employer. |

### Return type

null (empty response body)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="verifyEmployersList"></a>
# **verifyEmployersList**
> List&lt;Employer&gt; verifyEmployersList()



API endpoint that allows Persons to be viewed or edited.

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.VerifyApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();
// Configure HTTP basic authorization: Basic
HttpBasicAuth Basic = (HttpBasicAuth) defaultClient.getAuthentication("Basic");
Basic.setUsername("YOUR USERNAME");
Basic.setPassword("YOUR PASSWORD");

VerifyApi apiInstance = new VerifyApi();
try {
    List<Employer> result = apiInstance.verifyEmployersList();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling VerifyApi#verifyEmployersList");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**List&lt;Employer&gt;**](Employer.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="verifyEmployersPartialUpdate"></a>
# **verifyEmployersPartialUpdate**
> Employer verifyEmployersPartialUpdate(body, id)



API endpoint that allows Persons to be viewed or edited.

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.VerifyApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();
// Configure HTTP basic authorization: Basic
HttpBasicAuth Basic = (HttpBasicAuth) defaultClient.getAuthentication("Basic");
Basic.setUsername("YOUR USERNAME");
Basic.setPassword("YOUR PASSWORD");

VerifyApi apiInstance = new VerifyApi();
Employer body = new Employer(); // Employer | 
UUID id = new UUID(); // UUID | A UUID string identifying this employer.
try {
    Employer result = apiInstance.verifyEmployersPartialUpdate(body, id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling VerifyApi#verifyEmployersPartialUpdate");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Employer**](Employer.md)|  |
 **id** | [**UUID**](.md)| A UUID string identifying this employer. |

### Return type

[**Employer**](Employer.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="verifyEmployersRead"></a>
# **verifyEmployersRead**
> Employer verifyEmployersRead(id)



API endpoint that allows Persons to be viewed or edited.

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.VerifyApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();
// Configure HTTP basic authorization: Basic
HttpBasicAuth Basic = (HttpBasicAuth) defaultClient.getAuthentication("Basic");
Basic.setUsername("YOUR USERNAME");
Basic.setPassword("YOUR PASSWORD");

VerifyApi apiInstance = new VerifyApi();
UUID id = new UUID(); // UUID | A UUID string identifying this employer.
try {
    Employer result = apiInstance.verifyEmployersRead(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling VerifyApi#verifyEmployersRead");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**UUID**](.md)| A UUID string identifying this employer. |

### Return type

[**Employer**](Employer.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="verifyEmployersUpdate"></a>
# **verifyEmployersUpdate**
> Employer verifyEmployersUpdate(body, id)



API endpoint that allows Persons to be viewed or edited.

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.VerifyApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();
// Configure HTTP basic authorization: Basic
HttpBasicAuth Basic = (HttpBasicAuth) defaultClient.getAuthentication("Basic");
Basic.setUsername("YOUR USERNAME");
Basic.setPassword("YOUR PASSWORD");

VerifyApi apiInstance = new VerifyApi();
Employer body = new Employer(); // Employer | 
UUID id = new UUID(); // UUID | A UUID string identifying this employer.
try {
    Employer result = apiInstance.verifyEmployersUpdate(body, id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling VerifyApi#verifyEmployersUpdate");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Employer**](Employer.md)|  |
 **id** | [**UUID**](.md)| A UUID string identifying this employer. |

### Return type

[**Employer**](Employer.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="verifyPeopleCreate"></a>
# **verifyPeopleCreate**
> Person verifyPeopleCreate(body)



API endpoint that allows Persons to be viewed or edited.

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.VerifyApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();
// Configure HTTP basic authorization: Basic
HttpBasicAuth Basic = (HttpBasicAuth) defaultClient.getAuthentication("Basic");
Basic.setUsername("YOUR USERNAME");
Basic.setPassword("YOUR PASSWORD");

VerifyApi apiInstance = new VerifyApi();
Person body = new Person(); // Person | 
try {
    Person result = apiInstance.verifyPeopleCreate(body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling VerifyApi#verifyPeopleCreate");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Person**](Person.md)|  |

### Return type

[**Person**](Person.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="verifyPeopleDelete"></a>
# **verifyPeopleDelete**
> verifyPeopleDelete(id)



API endpoint that allows Persons to be viewed or edited.

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.VerifyApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();
// Configure HTTP basic authorization: Basic
HttpBasicAuth Basic = (HttpBasicAuth) defaultClient.getAuthentication("Basic");
Basic.setUsername("YOUR USERNAME");
Basic.setPassword("YOUR PASSWORD");

VerifyApi apiInstance = new VerifyApi();
UUID id = new UUID(); // UUID | A UUID string identifying this person.
try {
    apiInstance.verifyPeopleDelete(id);
} catch (ApiException e) {
    System.err.println("Exception when calling VerifyApi#verifyPeopleDelete");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**UUID**](.md)| A UUID string identifying this person. |

### Return type

null (empty response body)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="verifyPeopleList"></a>
# **verifyPeopleList**
> List&lt;Person&gt; verifyPeopleList()



API endpoint that allows Persons to be viewed or edited.

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.VerifyApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();
// Configure HTTP basic authorization: Basic
HttpBasicAuth Basic = (HttpBasicAuth) defaultClient.getAuthentication("Basic");
Basic.setUsername("YOUR USERNAME");
Basic.setPassword("YOUR PASSWORD");

VerifyApi apiInstance = new VerifyApi();
try {
    List<Person> result = apiInstance.verifyPeopleList();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling VerifyApi#verifyPeopleList");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**List&lt;Person&gt;**](Person.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="verifyPeoplePartialUpdate"></a>
# **verifyPeoplePartialUpdate**
> Person verifyPeoplePartialUpdate(body, id)



API endpoint that allows Persons to be viewed or edited.

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.VerifyApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();
// Configure HTTP basic authorization: Basic
HttpBasicAuth Basic = (HttpBasicAuth) defaultClient.getAuthentication("Basic");
Basic.setUsername("YOUR USERNAME");
Basic.setPassword("YOUR PASSWORD");

VerifyApi apiInstance = new VerifyApi();
Person body = new Person(); // Person | 
UUID id = new UUID(); // UUID | A UUID string identifying this person.
try {
    Person result = apiInstance.verifyPeoplePartialUpdate(body, id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling VerifyApi#verifyPeoplePartialUpdate");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Person**](Person.md)|  |
 **id** | [**UUID**](.md)| A UUID string identifying this person. |

### Return type

[**Person**](Person.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="verifyPeopleRead"></a>
# **verifyPeopleRead**
> Person verifyPeopleRead(id)



API endpoint that allows Persons to be viewed or edited.

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.VerifyApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();
// Configure HTTP basic authorization: Basic
HttpBasicAuth Basic = (HttpBasicAuth) defaultClient.getAuthentication("Basic");
Basic.setUsername("YOUR USERNAME");
Basic.setPassword("YOUR PASSWORD");

VerifyApi apiInstance = new VerifyApi();
UUID id = new UUID(); // UUID | A UUID string identifying this person.
try {
    Person result = apiInstance.verifyPeopleRead(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling VerifyApi#verifyPeopleRead");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**UUID**](.md)| A UUID string identifying this person. |

### Return type

[**Person**](Person.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="verifyPeopleUpdate"></a>
# **verifyPeopleUpdate**
> Person verifyPeopleUpdate(body, id)



API endpoint that allows Persons to be viewed or edited.

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.VerifyApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();
// Configure HTTP basic authorization: Basic
HttpBasicAuth Basic = (HttpBasicAuth) defaultClient.getAuthentication("Basic");
Basic.setUsername("YOUR USERNAME");
Basic.setPassword("YOUR PASSWORD");

VerifyApi apiInstance = new VerifyApi();
Person body = new Person(); // Person | 
UUID id = new UUID(); // UUID | A UUID string identifying this person.
try {
    Person result = apiInstance.verifyPeopleUpdate(body, id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling VerifyApi#verifyPeopleUpdate");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Person**](Person.md)|  |
 **id** | [**UUID**](.md)| A UUID string identifying this person. |

### Return type

[**Person**](Person.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

