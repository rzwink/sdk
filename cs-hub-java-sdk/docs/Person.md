# Person

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | [**UUID**](UUID.md) |  |  [optional]
**firstName** | **String** |  | 
**lastName** | **String** |  | 
**employments** | [**List&lt;PersonEmployment&gt;**](PersonEmployment.md) |  | 
