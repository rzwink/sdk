# PersonEmploymentPaystatements

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | [**UUID**](UUID.md) |  |  [optional]
**payDate** | [**LocalDate**](LocalDate.md) |  | 
**payPeriodStart** | [**LocalDate**](LocalDate.md) |  |  [optional]
**payPeriodEnd** | [**LocalDate**](LocalDate.md) |  |  [optional]
**netPayAmountCurrency** | **String** |  |  [optional]
**netPayAmount** | **String** |  |  [optional]
**grossPayAmountCurrency** | **String** |  |  [optional]
**grossPayAmount** | **String** |  |  [optional]
**grossPayYtdAmountCurrency** | **String** |  |  [optional]
**grossPayYtdAmount** | **String** |  |  [optional]
**totalHours** | **Integer** |  | 
**createdAt** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**updatedAt** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**employment** | [**UUID**](UUID.md) |  | 
