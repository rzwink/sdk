# PersonEmployment

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | [**UUID**](UUID.md) |  |  [optional]
**employer** | [**Employer**](Employer.md) |  | 
**monthsOfAvailableData** | **Integer** |  |  [optional]
**estimatedBaseSalary** | **String** |  | 
**payFrequency** | **String** |  |  [optional]
**paystatements** | [**List&lt;PersonEmploymentPaystatements&gt;**](PersonEmploymentPaystatements.md) |  |  [optional]
**startAt** | [**LocalDate**](LocalDate.md) |  |  [optional]
**endAt** | [**LocalDate**](LocalDate.md) |  |  [optional]
