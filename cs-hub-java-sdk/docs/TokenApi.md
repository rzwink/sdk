# TokenApi

All URIs are relative to *http://localhost:8000/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**tokenCreate**](TokenApi.md#tokenCreate) | **POST** /token/ | 
[**tokenRefreshCreate**](TokenApi.md#tokenRefreshCreate) | **POST** /token/refresh/ | 
[**tokenVerifyCreate**](TokenApi.md#tokenVerifyCreate) | **POST** /token/verify/ | 

<a name="tokenCreate"></a>
# **tokenCreate**
> TokenObtainPair tokenCreate(body)



Takes a set of user credentials and returns an access and refresh JSON web token pair to prove the authentication of those credentials.

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.TokenApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();
// Configure HTTP basic authorization: Basic
HttpBasicAuth Basic = (HttpBasicAuth) defaultClient.getAuthentication("Basic");
Basic.setUsername("YOUR USERNAME");
Basic.setPassword("YOUR PASSWORD");

TokenApi apiInstance = new TokenApi();
TokenObtainPair body = new TokenObtainPair(); // TokenObtainPair | 
try {
    TokenObtainPair result = apiInstance.tokenCreate(body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling TokenApi#tokenCreate");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**TokenObtainPair**](TokenObtainPair.md)|  |

### Return type

[**TokenObtainPair**](TokenObtainPair.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="tokenRefreshCreate"></a>
# **tokenRefreshCreate**
> TokenRefresh tokenRefreshCreate(body)



Takes a refresh type JSON web token and returns an access type JSON web token if the refresh token is valid.

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.TokenApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();
// Configure HTTP basic authorization: Basic
HttpBasicAuth Basic = (HttpBasicAuth) defaultClient.getAuthentication("Basic");
Basic.setUsername("YOUR USERNAME");
Basic.setPassword("YOUR PASSWORD");

TokenApi apiInstance = new TokenApi();
TokenRefresh body = new TokenRefresh(); // TokenRefresh | 
try {
    TokenRefresh result = apiInstance.tokenRefreshCreate(body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling TokenApi#tokenRefreshCreate");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**TokenRefresh**](TokenRefresh.md)|  |

### Return type

[**TokenRefresh**](TokenRefresh.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="tokenVerifyCreate"></a>
# **tokenVerifyCreate**
> TokenVerify tokenVerifyCreate(body)



Takes a token and indicates if it is valid.  This view provides no information about a token&#x27;s fitness for a particular use.

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.TokenApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();
// Configure HTTP basic authorization: Basic
HttpBasicAuth Basic = (HttpBasicAuth) defaultClient.getAuthentication("Basic");
Basic.setUsername("YOUR USERNAME");
Basic.setPassword("YOUR PASSWORD");

TokenApi apiInstance = new TokenApi();
TokenVerify body = new TokenVerify(); // TokenVerify | 
try {
    TokenVerify result = apiInstance.tokenVerifyCreate(body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling TokenApi#tokenVerifyCreate");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**TokenVerify**](TokenVerify.md)|  |

### Return type

[**TokenVerify**](TokenVerify.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

